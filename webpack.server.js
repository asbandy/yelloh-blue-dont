const path = require('path');
const merge = require('webpack-merge');
const baseConfig = require('./webpack.base.js');
const webpackNodeExternals = require('webpack-node-externals');

const config = {
  target: 'node',
  node: {
  __dirname: false
  },
  entry: './src/server.js',
  output: {
    filename: 'server-bundle.js',
    path: path.resolve(__dirname, 'public')
  },
  externals: [webpackNodeExternals()]
};

module.exports = merge(baseConfig, config);