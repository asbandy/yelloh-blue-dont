import 'babel-polyfill';
import express from 'express';
import { matchRoutes } from 'react-router-config';
import routes from './client/routes';
import renderer from './lib/render';
import createStore from './lib/createStore';
//
// Application constants and definitions
//
const app = express();
const HTTP_PORT = process.env.HTTP_PORT || 8080;
//
// Express static services
//
app.use(express.static('public'));
app.use(express.static('static'));
app.use('/favicon.ico', express.static('static/favicons/favicon.ico'));
//
// Routes
//
app.get('*', (req, res) => {
  const store = createStore(req);
  const promises = matchRoutes(routes, req.path)
    .map(({ route }) => {
      return route.loadData ? route.loadData(store) : null;
    })
    .map(promise => {
      if (promise) {
        return new Promise((resolve, reject) => {
          promise.then(resolve).catch(resolve);
        });
      }
    });
  Promise.all(promises).then(() => {
    const context = {};
    const content = renderer(req, store, context);
    if (context.url) {
      return res.redirect(301, context.url);
    }
    if (context.notFound) {
      res.status(404);
    }
    res.send(content);
  });
});
//
// Start services
//
app.listen(HTTP_PORT, () => {
  console.log(`DONT HTTP service running on ${HTTP_PORT}.`);
});