import React, { Component } from 'react';
import marked from 'marked';
import { connect } from 'react-redux';
import { getFile } from '../actions';

class Entry extends Component {
  constructor(props) {
    super(props);
    props.getFile(props.requestPath, props.fileName);
  }
 
  render() {
    return marked(this.props.file.__content);
  }
}
function mapStateToProps(state, ownProps) {
  console.log(ownProps.reqPath, ownProps.fileName, state.filePayloads);
  if (state.filePayloads) {
    return { file: state.filePayloads.find( filePayload => filePayload.reqPath === own.props.reqPath && filePayload.fileName === own.props.fileName) };
  } 
}
export default connect(mapStateToProps, { getFile })(Entry);