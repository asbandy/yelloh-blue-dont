import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getFiles } from '../actions';
import Entry from '../components/Entry';

class Composite extends Component {
  constructor(props) {
    super(props);
    props.getFiles(props.requestPath);
  }
  render() {
    console.log('Composite.render props', this.props);
    return "Hi from composite";
//     return this.props.filesList
//       ? this.props.filesList.map(fileName => <Entry reqPath={ '/' } fileName={ fileName } />)
//       : null;
  }
}
function mapStateToProps(state) {
  console.log('!!! Composite#mapStateToProps state', state);
  return { fileNames: state.fileNames };
}
export default connect(mapStateToProps, { getFiles })(Composite);