import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getControl } from '../actions';
import Composite from '../components/Composite';

class Page extends Component {
  componentDidMount() {
    this.props.getControl();
  }
  render() {
    console.log("Page.render this.props.control", this.props.control);
    return (
      <div>
      { this.props.control
          ? {
              COMPOSED_BY_DATE: <Composite/>,
            }[this.props.control.format]
          : 'Hello world.'
      }
      </div>
    );
  }
}
function mapStateToProps(state) {
  console.log('!!! Page#mapStateToProps state', state);
  return { control: state.control };
}
function loadData(store) {
  return store.dispatch(getControl());
}
export default {
  loadData,
  component: connect(mapStateToProps, { getControl })(Page)
};