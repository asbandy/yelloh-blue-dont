import React from 'react';
import App from './App';
import Page from './pages/Page';

export default [
  {
    ...App,
    routes: [
      {
        ...Page,
        path: '/',
        exact: true
      }
    ]
  }
];