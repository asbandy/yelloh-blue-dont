import { safeLoadFront } from 'yaml-front-matter';
import path from 'path';
import fs from 'fs';
//
// Constants
//
export const ERROR = 'ERROR';
export const CONTROL_RECEIVED = 'CONTROL_RECEIVED';
export const FILES_LIST_RECEIVED = 'FILES_LIST_RECEIVED';
export const FILE_CONTENT_RECEIVED = 'FILE_CONTENT_RECEIVED';
const DOCS_FOLDER = process.env.DOCS_FOLDER || 'docs';
const CONTROL_FILENAME = process.env.CONTROL_FILENAME || 'control.yml';
//
// Actions
//
export const getControl = (reqPath = '/') => (dispatch, getState, Api) => {
  try {
    const controlPath = path.join(__dirname, '..', DOCS_FOLDER, reqPath, CONTROL_FILENAME);
    const controlFile = fs.readFileSync(controlPath).toString();
    const control = safeLoadFront(controlFile);
    dispatch({ type: CONTROL_RECEIVED, control });
  } catch (error) {
    console.log('action error', error);
    dispatch({ type: ERROR, error, reqPath });
  }
}
export const getFiles = (reqPath = '/') => (dispatch, getState, Api) => {
  console.log('actions#getFiles', reqPath);
  try {
    const filesPath = path.join(__dirname, '..', DOCS_FOLDER, reqPath);
    const filesList = fs.readdirSync(filesPath).filter(fileName => fileName.includes('.md'));
    console.log('actions#getFiles got dir output', filesList);
    dispatch({ type: FILES_LIST_RECEIVED, reqPath, filesList  })
  } catch (error) {
    console.log('action error', error);
    dispath({ type: ERROR, error, reqPath });
  }
}
export const getFile = (reqPath = '/', fileName) => (dispatch, getState, Api) => {
  try {
    const filePath = path.join(__dirname, '..', DOCS_FOLDER, reqPath, fileName);
    const filePayload = safeLoadFront(filePath);
    dispatch({ type: FILE_CONTENT_RECEIVED, reqPath, fileName, filePayload });
  } catch (error) {
    console.log('action error', error);
    dispath({ type: ERROR, error, reqPath, fileName });
  }
}
