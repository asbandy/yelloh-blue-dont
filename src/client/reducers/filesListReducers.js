import { FILES_LIST_RECEIVED } from '../actions';

export default (state = [], action) => {
  console.log('fileListReducers state action\n', state, action);
  switch (action.type) {
    case FILES_LIST_RECEIVED:
      console.log('filesListReducers got FILES_LIST_RECEIVED', action, action.filesList);
      return [].concat(action.filesList);
      //return action.filesList.slice(0);
    default:
      return state;
  }
};