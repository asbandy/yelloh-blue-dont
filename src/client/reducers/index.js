import { combineReducers } from 'redux';
import controlReducers from './controlReducers';
import filesListReducers from './filesListReducers';
import filePayloadReducers from './filePayloadReducers';
import ERROR from '../actions';

const errorReducers = (state = [], action) => {
  switch (action.type) {
    case ERROR:
      // TODO Introduce more robust error handling (and outside of reducers);
      console.log('Action Error\n', action.error);
      return action.error;
    default:
      return state;
  }
};

export default combineReducers({
  error: errorReducers,
  control: controlReducers,
  fileNames: filesListReducers,
  filePayloads: filePayloadReducers
});