import { CONTROL_RECEIVED } from '../actions';

export default (state = [], action) => {
  switch (action.type) {
    case CONTROL_RECEIVED:
      return action.control;
    default:
      return state;
  }
};