import { FILE_CONTENT_RECEIVED } from '../actions';

export default (state = [], action) => {
  switch (action.type) {
    case FILE_CONTENT_RECEIVED:
      return [ ...state.filePayloads, { reqPath: action.reqPath, fileName: action.fileName, filePayload: action.filePayload } ];
    default:
      return state;
  }
};