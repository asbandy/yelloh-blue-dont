import React from 'react';
import { renderRoutes } from 'react-router-config';
import { getControl } from './actions';

const App = ({ route }) => {
  return renderRoutes(route.routes);
};

export default {
  component: App,
};